<?php

namespace Spotawheel\BabelHelper;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

class BabelHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/babelhelper.php' => config_path('babelhelper.php'),
        ]);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/babelhelper.php', 'babelhelper');

        $this->app->singleton(BabelHelper::class);

        $this->app->alias(BabelHelper::class, 'babelhelper');

    }

}
