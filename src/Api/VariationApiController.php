<?php

namespace Spotawheel\BabelHelper\Api;

use Spotawheel\BabelHelper\Api\RequestHelper;

class VariationApiController
{
    public static function getVariations($type = null, $key = null, $options = [])
    {
        $endpoint = self::buildEndpointUrl($type, $key);
        $request = new RequestHelper();
        $response = $request->makeRequest(
            $endpoint,
            'GET',
            $options
        );

        return $response['response'];
    }

    private static function buildEndpointUrl($type, $key)
    {
        $endpoint = "/api/v1/variations";

        if (isset($type)) {
            $endpoint = $endpoint . "/{$type}";
            if (isset($key)) {
                $endpoint = $endpoint . "/{$key}";
            }
        }

        return $endpoint;





//        if (!isset($type) && !isset($key)) {
//            $endpoint = "/api/v1/variations";
//        }
//
//        if (isset($type)) {
//            $endpoint = "/api/v1/variations/{$type}";
//        }
//
//        if (isset($type) && isset($key)) {
//            $endpoint = "/api/v1/variations/{$type}/{$key}";
//        }
//
//        return $endpoint;
    }
}