<?php

namespace Spotawheel\BabelHelper\Api;

use GuzzleHttp\Client;

class RequestHelper
{
    private $token;

    function __construct()
    {
        $this->token = self::getToken();
    }

    /**
     * @param $endpoint
     * @param $method
     * @param $options
     * @return array
     */
    public function makeRequest($endpoint, $method = 'GET', $options = [])
    {
        $client = new Client();
        $uri = config('babelhelper.connection.endpoint') . $endpoint;

        $requestOptions = [
            'headers' => [
                'Authorization' => "Bearer {$this->token->access_token}",
                'Content-Type' => 'application/json',
                'Cache-Control' => 'no-cache',
                'Accept' => 'application/json',
            ],
        ];
        if (isset($options['only_trashed']) && $options['only_trashed'] == 'true') {
            $uri = config('babelhelper.connection.endpoint') . $endpoint . '?only_trashed=true';
        }

        try {
            $response = $client->request(
                $method,
                $uri,
                $requestOptions
            );
            return [
                'is_successful' => true,
                'response' => json_decode($response->getBody()->getContents(), true),
            ];
        } catch (\Exception $e) {
            \Log::error("[BabelHelper] Unable to fetch json {$e->getMessage()}");
            return [
                'is_successful' => false,
                'response' => $e->getMessage(),
            ];
        }
    }

    /**
     * Get a token for the given credentials.
     * @return string|null
     */
    private function getToken()
    {
        $url = config('babelhelper.connection.endpoint') . '/oauth/token';
        $client = new Client();
        $requestOptions = [
            'form_params' => [
                'client_id' => config('babelhelper.connection.client_id'),
                'client_secret' => config('babelhelper.connection.client_secret'),
                'grant_type' => 'password',
                'username' => config('babelhelper.connection.username'),
                'password' => config('babelhelper.connection.password'),
            ],
        ];

        try {
            $response = $client->request('POST', $url, $requestOptions);
            return json_decode($response->getBody());
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            \Log::error("[BabelHelper] Unable to get token {$e->getMessage()}");
            return null;
        }
    }
}