<?php

namespace Spotawheel\BabelHelper;

use Cache;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class BabelHelper
{

    private $scope_id;
    private $token;

    function __construct()
    {
        $this->scope_id = self::getScopeId();
        $this->token = self::getToken();
    }

    /**
     * Change the current scope
     * @param $scope_id
     */
    public function setScope($scope_id)
    {
        $this->scope_id = $scope_id;
    }

    /**
     * Get a token for the given credentials.
     * @return string|null
     */
    private static function getToken()
    {
        $url = config('babelhelper.connection.endpoint').'/oauth/token';
        $client = new Client();
        $requestOptions = [
            'form_params' => [
                'client_id' => config('babelhelper.connection.client_id'),
                'client_secret' => config('babelhelper.connection.client_secret'),
                'grant_type' => 'password',
                'username' => config('babelhelper.connection.username'),
                'password' => config('babelhelper.connection.password'),
            ],
        ];

        try {
            $response = $client->request('POST', $url, $requestOptions);
            return json_decode($response->getBody());
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            \Log::error("[BabelHelper] Unable to get token {$e->getMessage()}");
            return null;
        }
    }

    /**
     *
     * @param $endpoint
     * @param  string  $method  GET,POST
     * @param  array  $options
     * @return array
     */
    private function makeRequest($endpoint, $method = 'GET', $options = [])
    {
        $client = new Client();

        $requestOptions = [
            'headers' => [
                'Authorization' => "Bearer {$this->token->access_token}",
                'Content-Type' => 'application/json',
                'Cache-Control' => 'no-cache',
                'Accept' => 'application/json',
            ],
        ];

        if ($method === 'POST') {
            $requestOptions['json'] = $options;
        }

        try {
            $response = $client->request(
                $method,
                config('babelhelper.connection.endpoint').$endpoint,
                $requestOptions
            );
            return [
                'is_successful' => true,
                'response' => json_decode($response->getBody()->getContents(), true),
            ];
        } catch (\Exception $e) {
            \Log::error("[BabelHelper] Unable to fetch json {$e->getMessage()}");
            return [
                'is_successful' => false,
                'response' => $e->getMessage(),
            ];
        }
    }

    private static function getJson($filename, $scope_id = null, $date = null, $decoded = false)
    {
        if ($scope_id === null) {
            $scope_id = self::getScopeId();
        }
        if ($scope_id === 'default') {
            $scope_id = null;
        }

        $path = config('babelhelper.storage_path')."/{$scope_id}/{$filename}.json";
        if ($date !== null) {
            $path = config('babelhelper.storage_path')."/{$date}/{$scope_id}/{$filename}.json";
            $filename .= "-{$date}";
        }

        $key = "babel-{$filename}-{$scope_id}";
        if ($decoded) {
            $key .= "-decoded";
        }
        return Cache::tags(['babel', 'babel-changes', "babel-scope-{$scope_id}"])
            ->remember($key, now()->addHours(23), function () use ($path, $scope_id, $decoded) {
                try {
                    $json = \Storage::disk(config('babelhelper.storage'))->get($path);
                    if ($decoded) {
                        return json_decode($json);
                    }
                    return $json;
                } catch (\Exception $e) {
                    return null;
                }
            });
    }

    public static function getCrawlerJson(
        $scope_id = null,
        $date = null,
        $decoded = false
    ) {
        $filename = 'make-models-revert';
        return self::getJson($filename, $scope_id, $date, $decoded);
    }

    public static function getMSMJson(
        $scope_id = null,
        $multiple_variations = false,
        $decoded = false
    ) {
        $filename = 'make-models-full';
        if ($multiple_variations) {
            $filename .= '-multiple';
        }
        return self::getJson($filename, $scope_id, null, $decoded);
    }

    public static function getMMJson(
        $scope_id = null,
        $multiple_variations = false,
        $decoded = false
    ) {
        $filename = 'make-models';
        if ($multiple_variations) {
            $filename .= '-multiple';
        }
        return self::getJson($filename, $scope_id, null, $decoded);
    }

    public static function getSpecificationJson(
        $type,
        $revert = false,
        $scope_id = null,
        $multiple_variations = false,
        $date = null,
        $decoded = false
    ) {
        $filename = "specifications-{$type}";
        if ($multiple_variations) {
            $filename .= '-multiple';
        }
        if ($revert) {
            $filename .= '-revert';
        }
        return self::getJson($filename, $scope_id, $date, $decoded);
    }

    public static function getDependentSpecificationJson(
        $type,
        $revert = false,
        $scope_id = null,
        $multiple_variations = false,
        $date = null,
        $decoded = false
    ) {
        $filename = "dependent-specifications-{$type}";
        if ($multiple_variations) {
            $filename .= '-multiple';
        }
        if ($revert) {
            $filename .= '-revert';
        }
        return self::getJson($filename, $scope_id, $date, $decoded);
    }

    public function importSpecificationVariation(
        $type,
        $value,
        $mapping_information = null,
        $key = null
    ) {
        $options = [
            'scope_id' => $this->scope_id,
            'value' => $value,
        ];

        if ($key !== null) {
            $options['key'] = $key;
        }

        if ($mapping_information !== null) {
            $options['mapping_information'] = $mapping_information;
        }

        $response = $this->makeRequest(
            "/api/v1/specifications/{$type}",
            'POST',
            $options
        );

        return $response['response'];
    }

    /**
     * Pass dependencies as a key-value pairs array.
     * For example:
     * [
     *     'country' => 'GR',
     * ]
     */
    public function importDependentSpecificationVariation(
        $type,
        $dependencies,
        $value,
        $mapping_information = null,
        $key = null
    ) {
        if ($dependencies instanceof Illuminate\Support\Collection) {
            $dependencies = $dependencies->toArray();
        }
        if (!count($dependencies)) {
            throw new Exception('No dependencies found. You should use importSpecificationVariation() instead.');
        }

        $options = [
            'scope_id' => $this->scope_id,
            'value' => $value,
            'dependencies' => $dependencies,
        ];

        if ($key !== null) {
            $options['key'] = $key;
        }

        if ($mapping_information !== null) {
            $options['mapping_information'] = $mapping_information;
        }

        $response = $this->makeRequest(
            "/api/v1/dependent-specifications/{$type}",
            'POST',
            $options
        );

        return $response['response'];
    }

    /**
     * Get a make-model json from babel, for the given scope_id.
     *
     * @param $value
     * @return string|null
     */
    public function importMakeVariation($value, $mapping_information = null)
    {
        $options = [
            'scope_id' => $this->scope_id,
            'value' => $value,
        ];

        if ($mapping_information !== null) {
            $options['mapping_information'] = $mapping_information;
        }

        $response = $this->makeRequest(
            '/api/v1/vehicle-make',
            'POST',
            $options
        );

        return $response['response'];
    }

    /**
     * Get a make-model json from babel, for the given scope_id.
     *
     * @param $make_key
     * @param $value
     * @return string|null
     */
    public function importModelVariation($make_key, $value, $mapping_information = null)
    {
        $options = [
            'scope_id' => $this->scope_id,
            'value' => $value,
        ];

        if ($mapping_information !== null) {
            $options['mapping_information'] = $mapping_information;
        }

        $response = $this->makeRequest(
            "/api/v1/vehicle-make/{$make_key}/vehicle-model",
            'POST',
            $options
        );

        return $response['response'];
    }

    /*
     * Bulk variations import
     */
    public function importVariations(array $variations)
    {
        $options = [
            'scope_id' => $this->scope_id,
            'data' => $variations,
        ];

        $response = $this->makeRequest(
            "/api/v1/variation",
            'POST',
            $options
        );

        return $response['response'];
    }

    /**
     * Find or Create Scope.
     *
     * @param $scope_key
     * @return false|mixed
     */
    function findOrCreateScope($scope_key)
    {
        $options = [
            'name' => $scope_key,
        ];

        $response = $this->makeRequest(
            "/api/v1/scopes",
            'POST',
            $options);

        if (!$response['is_successful']) {
            return false;
        }

        return $response['response']['data'];
    }

    /**
     * Generate json for the given scope.
     */
    function generateScopeJsons(): bool
    {
        $response = $this->makeRequest(
            "/api/v1/scopes/{$this->scope_id}/exportJson?sync",
            'GET');

        if (!$response['is_successful']) {
            \Log::error($response);
            return false;
        }

        return true;
    }

    public static function getSpecificationAtttributes($type, $value, $scope_id = null)
    {
        $value = self::formatStrCaseAccent($value);
        $array = self::getSpecificationJson($type, true, $scope_id, true, null, true);
        return $array->{$value} ?? null;
    }

    public static function getSpecificationKey($type, $value, $scope_id = null)
    {
        $value = self::formatStrCaseAccent($value);
        $array = self::getSpecificationJson($type, true, $scope_id, true, null, true);
        return $array->{$value}->key ?? null;
    }

    public static function getSpecificationValue($type, $value, $scope_id = null)
    {
        return Cache::remember("getSpecificationValue-$type-$value-$scope_id", 3600, function () use ($type, $value, $scope_id) {
            $array = self::getSpecificationJson($type, false, $scope_id, false, null, true);
            return $array->{$value}->specification ?? null;
        });
    }

    public static function getSpecificationValues($type, $value, $scope_id = null)
    {
        $array = self::getSpecificationJson($type, false, $scope_id, true, null, true);
        return $array->{$value}->specification ?? null;
    }

    public static function getDependentSpecificationKey($type, $dependencies, $value, $scope_id = null)
    {
        $value = self::formatStrCaseAccent($value);
        $array = self::getDependentSpecificationJson($type, true, $scope_id, true, null, true);
        if (!isset($array->{$value})) {
            return null;
        }
        $candidates_pool = $array->{$value}->keys;
        $candidates = self::filterDependentSpecificationCandidates($candidates_pool, $dependencies);
        return $candidates->first()->key ?? null;
    }

    public static function getDependentSpecificationValue($type, $dependencies, $value, $scope_id = null)
    {
        $array = self::getDependentSpecificationJson($type, false, $scope_id, false, null, true);
        if (!isset($array->{$value})) {
            return null;
        }
        $candidates_pool = $array->{$value}->dependent_specifications;
        $candidates = self::filterDependentSpecificationCandidates($candidates_pool, $dependencies);
        return $candidates->first()->dependent_specification ?? null;
    }

    private static function filterDependentSpecificationCandidates($collection, $dependencies)
    {
        return collect($collection)->filter(function ($candidate) use ($dependencies) {
            foreach ($dependencies as $key => $value) {
                $key = \Str::snake($key);
                if (($candidate->dependencies->{$key} ?? null) != $value) {
                    return false;
                }
                return true;
            }
        });
    }

    public static function getMakeKey($value, $scope_id = null)
    {
        $value = self::formatStrCaseAccent($value);
        $array = self::getCrawlerJson($scope_id, null, true);
        return $array->{$value}->make_key ?? null;
    }

    public static function getMakeValue($make_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, false, true);
        return $array->{$make_key}->make ?? null;
    }

    public static function getMakeValues($make_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, true, true);
        return $array->{$make_key}->make ?? null;
    }

    public static function getModelKey($make, $value, $scope_id = null)
    {
        $make = self::formatStrCaseAccent($make);
        $value = self::formatStrCaseAccent($value);
        $array = self::getCrawlerJson($scope_id, null, true);
        return $array->{$make}->models->{$value}->model_key ?? null;
    }

    public static function getModelValue($make_key, $model_key, $scope_id = null)
    {
        $array = self::getMMJson($scope_id, false, true);
        return $array->{$make_key}->models->{$model_key}->model ?? null;
    }

    public static function getModelValues($make_key, $model_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, true, true);
        $model_data = self::getModelDetails(
            self::getMakeValue($make_key, $scope_id),
            self::getModelValue($make_key, $model_key, $scope_id),
            $scope_id
        );
        if ($model_data['series_key'] === null) {
            return $array->{$make_key}->models->{$model_key}->model ?? null;
        }
        return $array->{$make_key}->series->{$model_data['series_key']}->models->{$model_key}->model ?? null;
    }

    public static function getSeriesKey($make, $value, $scope_id = null)
    {
        $make = self::formatStrCaseAccent($make);
        $value = self::formatStrCaseAccent($value);
        $array = self::getCrawlerJson($scope_id, null, true);
        return $array->{$make}->series->{$value}->series_key ?? null;
    }

    public static function getSeriesValue($make_key, $series_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, false, true);
        return $array->{$make_key}->series->{$series_key}->series ?? null;
    }

    public static function getSeriesValues($make_key, $series_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, true, true);
        return $array->{$make_key}->series->{$series_key}->series ?? null;
    }

    public static function getSeriesValuesForMake($make_key, $scope_id = null)
    {
        $array = self::getMSMJson($scope_id, true, true);
        return collect($array->{$make_key}->series ?? [])->map(function ($item) {
            return $item->series;
        });
    }

    public static function hasSeries($make_key, $scope_id = null): bool
    {
        $array = self::getMSMJson($scope_id, true, true);
        return isset($array->{$make_key}->series);
    }

    public static function getModelDetails($make, $value, $scope_id = null)
    {
        $make = self::formatStrCaseAccent($make);
        $value = self::formatStrCaseAccent($value);
        $array = self::getCrawlerJson($scope_id, null, true);
        return [
            'model_key' => $array->{$make}->models->{$value}->model_key ?? null,
            'series' => isset($array->{$make}->models->{$value}->series) ? last($array->{$make}->models->{$value}->series) : null,
            'series_key' => $array->{$make}->models->{$value}->series_key ?? null,
        ];
    }

    public static function findModelByTitle($title, $make_key, $scope_id = null, $series_key = null)
    {
        $candidates = [];
        // Get json
        $make_model_revert = json_decode(self::getCrawlerJson($scope_id), true);

        // format make
        $make = is_array(self::getMakeValue($make_key, $scope_id))
            ? self::getMakeValue($make_key, $scope_id)[0]
            : self::getMakeValue($make_key, $scope_id);

        if (is_null($make)) {
            return [];
        }
        $make = self::formatStrCaseAccent($make);

        // format title
        $titles = is_array($title) ? $title : [$title];

        foreach ($titles as $title) {
            $title = self::formatStrCaseAccent($title);
            $title = \Str::replace($make, '', $title);
            $title = " {$title} ";

            // find series in title
            if ($series_key === null) {
                $series = collect($make_model_revert[$make]['series'])->sortByDesc(function ($item) {
                    return strlen($item['series_key']);
                });

                foreach ($series as $series_value => $series_data) {
                    if (\Str::contains($title, $series_value)) {
                        $candidates[] = [
                            'model' => null,
                            'model_key' => null,
                            'series' => $series_value,
                            'series_key' => $series_data['series_key'],
                        ];
                    }
                }
            }

            // find model in title
            $models = collect($make_model_revert[$make]['models']);
            if ($series_key !== null) {
                $models = $models->filter(function ($item) use ($series_key) {
                    return isset($item['series_key']) && $item['series_key'] === $series_key;
                });
            }
            $models = $models->sortByDesc(function ($item) {
                return strlen($item['model_key']);
            });
            foreach ($models as $model_value => $model_data) {
                if (Str::contains($title, " {$model_value}") !== false) {
                    $series = $model_data['series'] ?? null;
                    $candidates[] = [
                        'model' => $model_value,
                        'model_key' => $model_data['model_key'],
                        'series' => is_array($series) ? $series[0] : $series,
                        'series_key' => $model_data['series_key'] ?? null,
                    ];
                }
            }
        }

        return $candidates;
    }

    public static function getScopeId()
    {
        return config('babelhelper.scope_id');
    }

    static function formatStrCaseAccent($value)
    {
        return mb_strtoupper(trim( (string) $value));
    }

    static function wordpress_remove_accents($string)
    {
        if (!preg_match('/[\x80-\xff]/', $string)) {
            return $string;
        }

        $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A',
            chr(195).chr(129) => 'A',
            chr(195).chr(130) => 'A',
            chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A',
            chr(195).chr(133) => 'A',
            chr(195).chr(135) => 'C',
            chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E',
            chr(195).chr(138) => 'E',
            chr(195).chr(139) => 'E',
            chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I',
            chr(195).chr(142) => 'I',
            chr(195).chr(143) => 'I',
            chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O',
            chr(195).chr(147) => 'O',
            chr(195).chr(148) => 'O',
            chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O',
            chr(195).chr(153) => 'U',
            chr(195).chr(154) => 'U',
            chr(195).chr(155) => 'U',
            chr(195).chr(156) => 'U',
            chr(195).chr(157) => 'Y',
            chr(195).chr(159) => 's',
            chr(195).chr(160) => 'a',
            chr(195).chr(161) => 'a',
            chr(195).chr(162) => 'a',
            chr(195).chr(163) => 'a',
            chr(195).chr(164) => 'a',
            chr(195).chr(165) => 'a',
            chr(195).chr(167) => 'c',
            chr(195).chr(168) => 'e',
            chr(195).chr(169) => 'e',
            chr(195).chr(170) => 'e',
            chr(195).chr(171) => 'e',
            chr(195).chr(172) => 'i',
            chr(195).chr(173) => 'i',
            chr(195).chr(174) => 'i',
            chr(195).chr(175) => 'i',
            chr(195).chr(177) => 'n',
            chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o',
            chr(195).chr(180) => 'o',
            chr(195).chr(181) => 'o',
            chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o',
            chr(195).chr(185) => 'u',
            chr(195).chr(186) => 'u',
            chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u',
            chr(195).chr(189) => 'y',
            chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A',
            chr(196).chr(129) => 'a',
            chr(196).chr(130) => 'A',
            chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A',
            chr(196).chr(133) => 'a',
            chr(196).chr(134) => 'C',
            chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C',
            chr(196).chr(137) => 'c',
            chr(196).chr(138) => 'C',
            chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C',
            chr(196).chr(141) => 'c',
            chr(196).chr(142) => 'D',
            chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D',
            chr(196).chr(145) => 'd',
            chr(196).chr(146) => 'E',
            chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E',
            chr(196).chr(149) => 'e',
            chr(196).chr(150) => 'E',
            chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E',
            chr(196).chr(153) => 'e',
            chr(196).chr(154) => 'E',
            chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G',
            chr(196).chr(157) => 'g',
            chr(196).chr(158) => 'G',
            chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G',
            chr(196).chr(161) => 'g',
            chr(196).chr(162) => 'G',
            chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H',
            chr(196).chr(165) => 'h',
            chr(196).chr(166) => 'H',
            chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I',
            chr(196).chr(169) => 'i',
            chr(196).chr(170) => 'I',
            chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I',
            chr(196).chr(173) => 'i',
            chr(196).chr(174) => 'I',
            chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I',
            chr(196).chr(177) => 'i',
            chr(196).chr(178) => 'IJ',
            chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J',
            chr(196).chr(181) => 'j',
            chr(196).chr(182) => 'K',
            chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k',
            chr(196).chr(185) => 'L',
            chr(196).chr(186) => 'l',
            chr(196).chr(187) => 'L',
            chr(196).chr(188) => 'l',
            chr(196).chr(189) => 'L',
            chr(196).chr(190) => 'l',
            chr(196).chr(191) => 'L',
            chr(197).chr(128) => 'l',
            chr(197).chr(129) => 'L',
            chr(197).chr(130) => 'l',
            chr(197).chr(131) => 'N',
            chr(197).chr(132) => 'n',
            chr(197).chr(133) => 'N',
            chr(197).chr(134) => 'n',
            chr(197).chr(135) => 'N',
            chr(197).chr(136) => 'n',
            chr(197).chr(137) => 'N',
            chr(197).chr(138) => 'n',
            chr(197).chr(139) => 'N',
            chr(197).chr(140) => 'O',
            chr(197).chr(141) => 'o',
            chr(197).chr(142) => 'O',
            chr(197).chr(143) => 'o',
            chr(197).chr(144) => 'O',
            chr(197).chr(145) => 'o',
            chr(197).chr(146) => 'OE',
            chr(197).chr(147) => 'oe',
            chr(197).chr(148) => 'R',
            chr(197).chr(149) => 'r',
            chr(197).chr(150) => 'R',
            chr(197).chr(151) => 'r',
            chr(197).chr(152) => 'R',
            chr(197).chr(153) => 'r',
            chr(197).chr(154) => 'S',
            chr(197).chr(155) => 's',
            chr(197).chr(156) => 'S',
            chr(197).chr(157) => 's',
            chr(197).chr(158) => 'S',
            chr(197).chr(159) => 's',
            chr(197).chr(160) => 'S',
            chr(197).chr(161) => 's',
            chr(197).chr(162) => 'T',
            chr(197).chr(163) => 't',
            chr(197).chr(164) => 'T',
            chr(197).chr(165) => 't',
            chr(197).chr(166) => 'T',
            chr(197).chr(167) => 't',
            chr(197).chr(168) => 'U',
            chr(197).chr(169) => 'u',
            chr(197).chr(170) => 'U',
            chr(197).chr(171) => 'u',
            chr(197).chr(172) => 'U',
            chr(197).chr(173) => 'u',
            chr(197).chr(174) => 'U',
            chr(197).chr(175) => 'u',
            chr(197).chr(176) => 'U',
            chr(197).chr(177) => 'u',
            chr(197).chr(178) => 'U',
            chr(197).chr(179) => 'u',
            chr(197).chr(180) => 'W',
            chr(197).chr(181) => 'w',
            chr(197).chr(182) => 'Y',
            chr(197).chr(183) => 'y',
            chr(197).chr(184) => 'Y',
            chr(197).chr(185) => 'Z',
            chr(197).chr(186) => 'z',
            chr(197).chr(187) => 'Z',
            chr(197).chr(188) => 'z',
            chr(197).chr(189) => 'Z',
            chr(197).chr(190) => 'z',
            chr(197).chr(191) => 's',
            'ά' => 'α',
            'έ' => 'ε',
            'ί' => 'ι',
            'ή' => 'η',
            'ύ' => 'υ',
            'ό' => 'ο',
            'ώ' => 'ω',
            'Ά' => 'Α',
            'Έ' => 'Ε',
            'Ί' => 'Ι',
            'Ή' => 'Η',
            'Ύ' => 'Υ',
            'Ό' => 'Ο',
            'Ώ' => 'Ω',
        );

        return strtr($string, $chars);
    }

    /*
     * Get the unmapped variation count per scope and type
     */
    public function getUnmappedVariationsCount(string $type): string
    {
        $response = $this->makeRequest(
            "/api/v1/variations/unmapped?type={$type}&scope_id={$this->scope_id}",
            'GET'
        );

        return $response['response'];
    }
}
