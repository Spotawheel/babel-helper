<?php

return [
    /*
     * The storage json should be stored.
     * i.e. local, s3
     */
    'storage' => 's3-babel',

    /*
     * The storage path jsons should be stored.
     * Usage example: \Storage::get("{$storage_path}/{$scope_id}/{$filename}.json")
     */
    'storage_path' => 'Exports/Json',

    /*
     * Babel oath credentials
     */
    'connection' => [
        'endpoint' => env('BABEL_ENDPOINT'),
        'client_id' => env('BABEL_CLIENT_ID'),
        'client_secret' => env('BABEL_CLIENT_SECRET'),
        'username' => env('BABEL_USERNAME'),
        'password' => env('BABEL_PASSWORD'),
    ],

    /*
     * Scope id
     */
    'scope_id' => null,
];
