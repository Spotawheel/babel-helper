# Babel Helper


## Installation

You can install the package via composer:

```bash
"repositories": [
    {
        "type": "git",
        "url":  "https://bitbucket.org/Spotawheel/babel-helper.git"
    }
],
"require": {
    "spotawheel/babel-helper": "*"
},
```

The package will automatically register itself.

## Usage

You can publish the package's configuration using this command:

```bash
php artisan vendor:publish --provider="Spotawheel\BabelHelper\BabelHelperServiceProvider"
```

This will add the `babelhelper.php` file in your config directory with the following contents:

## Available functions

Remember to include
```php
use Spotawheel\BabelHelper\BabelHelper;
````

### Scope functions
```php
use Spotawheel\BabelHelper\BabelHelper;
$helper = new BabelHelper();
$helper->findOrCreateScope($scope_key);
```

### Make functions

```php
BabelHelper::getMakeKey($value, $scope_id = null);
```
**returns** make key (string) or null

**requires** `$helper->fetchCrawlerJson()`

```php
BabelHelper::getMakeValue($make_key, $scope_id = null);
```
**returns** make value (string) or null

**requires** `$helper->fetchMSMJson()`

```php
BabelHelper::getMakeValues($make_key, $scope_id = null);
```
**returns** make values (array) or null

**requires** `$helper->fetchMSMJson(true, true)`

### Model functions

```php
BabelHelper::getModelKey($make, $value, $scope_id = null);
```
**returns** model key (string) or null

**requires** `$helper->fetchCrawlerJson()`

```php
BabelHelper::getModelValue($make_key, $model_key, $scope_id = null);
```
**returns** model value (string) or null

**requires** `$helper->fetchMMJson()`

```php
BabelHelper::getModelValues($make_key, $model_key, $scope_id = null);
```
**returns** model values (array) or null

**requires** `$helper->fetchMSMJson(true, true)`

```php
BabelHelper::getModelDetails($make, $value, $scope_id = null);
```
**returns** the following array
```php
[
    'model_key' => '...', // model_key or null,
    'series' => '...', // series value or null,
    'series_key' => '...', // series_key or null,
]
```

**requires** `$helper->fetchCrawlerJson()`

### Series functions

```php
BabelHelper::getSeriesKey($make, $value, $scope_id = null);
```
**returns** series key (string) or null

**requires** `$helper->fetchCrawlerJson()`

```php
BabelHelper::getSeriesValue($make_key, $series_key, $scope_id = null);
```
**returns** series value (string) or null

**requires** `$helper->fetchMSMJson()`

```php
BabelHelper::getSeriesValues($make_key, $series_key, $scope_id = null);
```
**returns** series values (array) or null

**requires** `$helper->fetchMSMJson(true, true)`

### Get total unmapped variations per scope

```php
$helper = new Spotawheel\BabelHelper\BabelHelper();
$helper->setScope($scope_id_or_key);
$helper->getUnmappedVariationsCount('fuel_type'); // make, model, fuel_type, etc.
```
**returns** int