# Changelog

All changes to `spotawheel/babel-helper` will be documented in this file.

## 2021-12-20
- findModelByTitle will return an array of results instead of the first result found

## 2021-12-10
- More performance improvements regarding json_decode

## 2021-12-01
- Performance improvements regarding json_decode

## 2021-10-16
- Add importVariations function for bulk import

## 2021-09-02
- Improve findModelByTitle function

## 2021-08-26
- Add generateScopeJsons function

## 2021-08-21
- Improve findModelByTitle function.

## 2021-07-29
- Added getUnmappedVariationsCount function.

## 2021-07-28
- Include changes jsons in the required functions

## 2021-07-26
- Add functions getSeriesValuesForMake, hasSeries

## 2021-07-08
- Minor fix

## 2021-07-06
- Add mapping_information option in variations import

## 2021-06-30
- Instead of locally save json files, ask them on the fly from s3 when needed
- Cache now uses 'babel' tag. 

## 2021-06-10
- Add `getSpecificationValues` functions, that return an array with all available variations.

## 2021-06-08
- Fixes in getModelValues

## 2021-06-03
- Add `getMakeValues`, `getSeriesValues`, `getModelValues` functions, that return an array with all available variations.
- README Documentation improvement 

## 2021-04-14
- Support default values export

## 2021-04-08
- Remove accent filter

## 2021-04-06
- bug fixes
- added getSeriesValue

## 2021-04-01
- Add support for Dependent Specifications

## 2021-03-09
- Fix in case-insensitive and accent-irrelevant

## 2021-02-15
- Make key searching case-insensitive and accent-irrelevant

## 2021-02-12
- Changed `importSpecificationVariation` to optionally accept key, and allow import of mapped variations
- Added function `findOrCreateScope` that creates scopes

## 2021-02-11
- Added support for php 8